//
//  SearchFilterVC.swift
//  OuulaTv
//
//  Created by Sevta on 21/07/19.
//  Copyright © 2019 Sevta. All rights reserved.
//

import UIKit

class SearchFilterVC: UIViewController {

    @IBOutlet var buttonFilterResults: UIButton!
    @IBOutlet var viewSearch: UIView!
    @IBOutlet var buttonClose: UIButton!
    @IBOutlet var tableViewSearch: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        viewSearch.layer.cornerRadius = 3.0
        viewSearch.clipsToBounds      = true
        
        buttonFilterResults.layer.cornerRadius = 18.0
        buttonFilterResults.clipsToBounds      = true
        
    }

    @IBAction func buttonCloseAction(_ sender: Any) {
        self.view.endEditing(true)
    }
}

extension SearchFilterVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterApplyCollectionViewCell", for: indexPath) as! FilterApplyCollectionViewCell
        
        cell.viewBg.layer.cornerRadius = 11.0
        cell.viewBg.clipsToBounds      = true
        
        return cell
        
    }
    
}

extension SearchFilterVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchFilterTableViewCell", for: indexPath) as! SearchFilterTableViewCell
        
        cell.selectionStyle = .none
        return cell
        
    }
    
}
