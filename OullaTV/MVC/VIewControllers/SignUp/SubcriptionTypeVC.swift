//
//  SubcriptionTypeVC.swift
//  OullaTV
//
//  Created by MAC on 20/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class SubcriptionTypeVC: UIViewController {
    
    @IBOutlet weak var signUpTableView: UITableView!
    @IBOutlet weak var proceedButton: UIButton!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    
    @IBOutlet weak var basicSubcriptionSelectedView: UIView!
    @IBOutlet weak var basicSubcriptionUnSelectedView: UIView!
    @IBOutlet weak var basicSubcriptionSelectedImageView: UIImageView!
    
    @IBOutlet weak var standardSubcriptionSelectedView: UIView!
    @IBOutlet weak var standardSubcriptionUnSelectedView: UIView!
    @IBOutlet weak var standardSubcriptionSelectedImageView: UIImageView!
    
    @IBOutlet weak var plusSubcriptionSelectedView: UIView!
    @IBOutlet weak var plusSubcriptionUnSelectedView: UIView!
    @IBOutlet weak var plusSubcriptionSelectedImageView: UIImageView!
    
    let unSelectedColor = UIColor.init(red: 22.0/255.0, green: 52.0/255.0, blue: 62.0/255.0, alpha: 1.0)
    let selectedColor = UIColor.init(red: 186.0/255.0, green: 24.0/255.0, blue: 39.0/255.0, alpha: 1.0)
    
    var selectedSubcriptionType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setUpView() {
        
        headerView.frame = CGRect.init(x: headerView.frame.origin.x, y: headerView.frame.origin.y, width: UIScreen.main.bounds.size.width, height: headerView.frame.size.height * scaleFactorX)
        
        footerView.frame = CGRect.init(x: footerView.frame.origin.x, y: footerView.frame.origin.y, width: UIScreen.main.bounds.size.width, height: footerView.frame.size.height * scaleFactorX)
        
        proceedButton.layer.borderColor = UIColor.init(red: 142.0/255.0, green: 89.0/255.0, blue: 177.0/255.0, alpha: 1.0).cgColor
        proceedButton.layer.borderWidth = 1.0
        proceedButton.layer.masksToBounds = true
        proceedButton.layer.cornerRadius = (proceedButton.frame.size.height / 2) * scaleFactorX
        
        signUpTableView.delegate = self
        signUpTableView.dataSource = self
        
        basicSubcriptionUnSelectedView.isHidden = false
        basicSubcriptionSelectedView.isHidden = true
        basicSubcriptionSelectedImageView.isHidden = true
        
        basicSubcriptionSelectedView.backgroundColor = selectedColor
        basicSubcriptionUnSelectedView.backgroundColor = unSelectedColor
        
        basicSubcriptionSelectedView.layer.cornerRadius = 3.0
        basicSubcriptionSelectedView.layer.borderWidth = 1.0
        basicSubcriptionSelectedView.layer.borderColor = UIColor.clear.cgColor
        basicSubcriptionSelectedView.layer.masksToBounds = false
        
        basicSubcriptionSelectedView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        basicSubcriptionSelectedView.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        basicSubcriptionSelectedView.layer.shadowOpacity = 1.0
        basicSubcriptionSelectedView.layer.shadowRadius = 4.0
        
        basicSubcriptionUnSelectedView.layer.cornerRadius = 3.0
        basicSubcriptionUnSelectedView.layer.borderWidth = 1.0
        basicSubcriptionUnSelectedView.layer.borderColor = UIColor.clear.cgColor
        basicSubcriptionUnSelectedView.layer.masksToBounds = true
        
        standardSubcriptionUnSelectedView.isHidden = false
        standardSubcriptionSelectedView.isHidden = true
        standardSubcriptionSelectedImageView.isHidden = true
        
        standardSubcriptionSelectedView.backgroundColor = selectedColor
        standardSubcriptionUnSelectedView.backgroundColor = unSelectedColor
        
        standardSubcriptionSelectedView.layer.cornerRadius = 3.0
        standardSubcriptionSelectedView.layer.borderWidth = 1.0
        standardSubcriptionSelectedView.layer.borderColor = UIColor.clear.cgColor
        standardSubcriptionSelectedView.layer.masksToBounds = false
        
        standardSubcriptionSelectedView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        standardSubcriptionSelectedView.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        standardSubcriptionSelectedView.layer.shadowOpacity = 1.0
        standardSubcriptionSelectedView.layer.shadowRadius = 4.0
        
        standardSubcriptionUnSelectedView.layer.cornerRadius = 3.0
        standardSubcriptionUnSelectedView.layer.borderWidth = 1.0
        standardSubcriptionUnSelectedView.layer.borderColor = UIColor.clear.cgColor
        standardSubcriptionUnSelectedView.layer.masksToBounds = true
        
        plusSubcriptionUnSelectedView.isHidden = false
        plusSubcriptionSelectedView.isHidden = true
        plusSubcriptionSelectedImageView.isHidden = true
        
        plusSubcriptionSelectedView.backgroundColor = selectedColor
        plusSubcriptionUnSelectedView.backgroundColor = unSelectedColor
        
        plusSubcriptionSelectedView.layer.cornerRadius = 3.0
        plusSubcriptionSelectedView.layer.borderWidth = 1.0
        plusSubcriptionSelectedView.layer.borderColor = UIColor.clear.cgColor
        plusSubcriptionSelectedView.layer.masksToBounds = false
        
        plusSubcriptionSelectedView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        plusSubcriptionSelectedView.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        plusSubcriptionSelectedView.layer.shadowOpacity = 1.0
        plusSubcriptionSelectedView.layer.shadowRadius = 4.0
        
        plusSubcriptionUnSelectedView.layer.cornerRadius = 3.0
        plusSubcriptionUnSelectedView.layer.borderWidth = 1.0
        plusSubcriptionUnSelectedView.layer.borderColor = UIColor.clear.cgColor
        plusSubcriptionUnSelectedView.layer.masksToBounds = true
    }
    
    @IBAction func basicSubcriptionTypeButtonClick(_ sender: Any) {
        
        selectedSubcriptionType = "basic"
        
        basicSubcriptionUnSelectedView.isHidden = true
        basicSubcriptionSelectedView.isHidden = false
        basicSubcriptionSelectedImageView.isHidden = false
        
        standardSubcriptionUnSelectedView.isHidden = false
        standardSubcriptionSelectedView.isHidden = true
        standardSubcriptionSelectedImageView.isHidden = true
        
        plusSubcriptionUnSelectedView.isHidden = false
        plusSubcriptionSelectedView.isHidden = true
        plusSubcriptionSelectedImageView.isHidden = true
    }
    
    @IBAction func standardSubcriptionTypeButtonClick(_ sender: Any) {
        
        selectedSubcriptionType = "standard"
        
        basicSubcriptionUnSelectedView.isHidden = false
        basicSubcriptionSelectedView.isHidden = true
        basicSubcriptionSelectedImageView.isHidden = true
        
        standardSubcriptionUnSelectedView.isHidden = true
        standardSubcriptionSelectedView.isHidden = false
        standardSubcriptionSelectedImageView.isHidden = false
        
        plusSubcriptionUnSelectedView.isHidden = false
        plusSubcriptionSelectedView.isHidden = true
        plusSubcriptionSelectedImageView.isHidden = true
    }
    
    @IBAction func plusSubcriptionTypeButtonClick(_ sender: Any) {
        
        selectedSubcriptionType = "plus"
        
        basicSubcriptionUnSelectedView.isHidden = false
        basicSubcriptionSelectedView.isHidden = true
        basicSubcriptionSelectedImageView.isHidden = true
        
        standardSubcriptionUnSelectedView.isHidden = false
        standardSubcriptionSelectedView.isHidden = true
        standardSubcriptionSelectedImageView.isHidden = true
        
        plusSubcriptionUnSelectedView.isHidden = true
        plusSubcriptionSelectedView.isHidden = false
        plusSubcriptionSelectedImageView.isHidden = false
    }
    
    @IBAction func proceedButtonClick(_ sender: Any) {
        let paymentDetailsVC = UIStoryboard.getSignUpStoryBoard().instantiateViewController(withIdentifier: "PaymentDetailsVC") as! PaymentDetailsVC
        self.navigationController?.pushViewController(paymentDetailsVC, animated: true)
    }
}

extension SubcriptionTypeVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "")!
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 0
    }
}
