//
//  SideMenuViewController.swift
//  OullaTV
//
//  Created by Sumit Sharma on 20/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func crossBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}

extension SideMenuViewController :  UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        
        cell.imgSeprator.isHidden = true

        switch indexPath.row {
        case 0:
            cell.imgSeprator.isHidden = false
            cell.labelText.text = "Preferences"
            break
        case 1:
            cell.labelText.text = "Details"
            break
        case 2:
            cell.labelText.text = "Subscription"
            break
        case 3:
            cell.labelText.text = "Devices"
            break
        case 4:
            cell.labelText.text = "Payment details"
            break
        case 5:
            cell.labelText.text = "Cancel My Account"
            break

        default:
            break
        }
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            let prefrenceVC = UIStoryboard.getSideMenuStoryBoard().instantiateViewController(withIdentifier: "PrefrenceViewController") as! PrefrenceViewController
            self.navigationController?.pushViewController(prefrenceVC, animated: true)
            
        case 1:
            let signUpDetailsVC = UIStoryboard.getSideMenuStoryBoard().instantiateViewController(withIdentifier: "MyDetailsViewController") as! MyDetailsViewController
            self.navigationController?.pushViewController(signUpDetailsVC, animated: true)

        case 2 :
            let subscriptionVC = UIStoryboard.getSideMenuStoryBoard().instantiateViewController(withIdentifier: "MySubscriptionViewController") as! MySubscriptionViewController
            self.navigationController?.pushViewController(subscriptionVC, animated: true)

        case 3:
            let deviceVC = UIStoryboard.getSideMenuStoryBoard().instantiateViewController(withIdentifier: "MyDeviceViewController") as! MyDeviceViewController
            self.navigationController?.pushViewController(deviceVC, animated: true)

        case 4:
            let paymntVC = UIStoryboard.getSideMenuStoryBoard().instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            self.navigationController?.pushViewController(paymntVC, animated: true)
        case 5:
            let cancleVC = UIStoryboard.getSideMenuStoryBoard().instantiateViewController(withIdentifier: "CancelAccountViewController") as! CancelAccountViewController
            self.navigationController?.pushViewController(cancleVC, animated: true)

        default:
            break
        }
        
    }
}
