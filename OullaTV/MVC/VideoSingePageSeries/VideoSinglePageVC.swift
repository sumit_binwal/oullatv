//
//  VideoSinglePageVC.swift
//  OuulaTv
//
//  Created by MAC on 21/07/19.
//  Copyright © 2019 Sevta. All rights reserved.
//

import UIKit

class VideoSinglePageVC: UIViewController {

    @IBOutlet var videoTableView: UITableView!
    
    @IBOutlet weak var resumeButton: UIButton!
    @IBOutlet weak var playButton: UIButton!

    @IBOutlet var headerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 84/255.0, blue: 102/255.0, alpha: 1.0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 142/255.0, green: 89/255.0, blue: 177/255.0, alpha: 1.0)
    }
    
    func setUpView() {
        
        headerView.frame = CGRect.init(x: headerView.frame.origin.x, y: headerView.frame.origin.y, width: UIScreen.main.bounds.size.width, height: headerView.frame.size.height * scaleFactorX)
        
        videoTableView.delegate = self
        videoTableView.dataSource = self
        
        resumeButton.layer.borderColor = UIColor.clear.cgColor
        resumeButton.layer.borderWidth = 1.0
        resumeButton.layer.masksToBounds = true
        resumeButton.layer.cornerRadius = (resumeButton.frame.size.height / 2) * scaleFactorX
        
        playButton.layer.borderColor = UIColor.white.cgColor
        playButton.layer.borderWidth = 1.0
        playButton.layer.masksToBounds = true
        playButton.layer.cornerRadius = (playButton.frame.size.height / 2) * scaleFactorX
    }
    
    @IBAction func resumeVideoButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func playFromBeginningButtonAction(_ sender: Any) {
        
    }
}

extension VideoSinglePageVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headrCell = tableView.dequeueReusableCell(withIdentifier: "VideoHeaderTableViewCell") as! VideoHeaderTableViewCell
        return headrCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamsTableViewCell") as! TeamsTableViewCell
        
        cell.liveBgView.isHidden = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100 * scaleFactorX
    }
}
