//
//  HomeViewController.swift
//  OullaTV
//
//  Created by Sumit Sharma on 18/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func signInBtnClicked(_ sender: UIButton) {
        
        let loginVC = UIStoryboard.getLoginStoryBoard().instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @IBAction func createAccountBtnClicked(_ sender: UIButton) {
        let signUpDetailsVC = UIStoryboard.getSignUpStoryBoard().instantiateViewController(withIdentifier: "SignUpDetailsVC") as! SignUpDetailsVC
        self.navigationController?.pushViewController(signUpDetailsVC, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
