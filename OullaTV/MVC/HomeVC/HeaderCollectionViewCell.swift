//
//  HeaderCollectionViewCell.swift
//  OuulaTv
//
//  Created by Sevta on 20/07/19.
//  Copyright © 2019 Sevta. All rights reserved.
//

import UIKit

class HeaderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageViewHeader: UIImageView!
}
