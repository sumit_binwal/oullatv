//
//  TeamsTableViewCell.swift
//  OuulaTv
//
//  Created by MAC on 21/07/19.
//  Copyright © 2019 Sevta. All rights reserved.
//

import UIKit

class TeamsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var teamImagView: UIImageView!
    @IBOutlet weak var liveBgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
        
        teamImagView.layer.borderColor = UIColor.clear.cgColor
        teamImagView.layer.borderWidth = 1.0
        teamImagView.layer.masksToBounds = true
        teamImagView.layer.cornerRadius = 3.0 * scaleFactorX
        
        liveBgView.layer.borderColor = UIColor.clear.cgColor
        liveBgView.layer.borderWidth = 1.0
        liveBgView.layer.masksToBounds = true
        liveBgView.layer.cornerRadius = 3.0 * scaleFactorX
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
