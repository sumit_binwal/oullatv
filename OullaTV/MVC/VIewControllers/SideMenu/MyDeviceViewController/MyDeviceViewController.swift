//
//  MyDeviceViewController.swift
//  OullaTV
//
//  Created by Sumit Sharma on 21/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class MyDeviceViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MyDeviceViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeviceCell") as! DeviceCell
        
        switch indexPath.row {
        case 0:
            cell.imgVwTop.isHidden = false
            cell.imgVwDevice.image = #imageLiteral(resourceName: "imgIphone")
            cell.lblTitle.text = "iPhone 7s"
            cell.btnDelete.setImage(#imageLiteral(resourceName: "dltBtnWhite"), for: .normal)
        case 1:
            cell.imgVwTop.isHidden = true
            cell.imgVwDevice.image = #imageLiteral(resourceName: "imgLaptop")
            cell.lblTitle.text = "Macbook"
            cell.btnDelete.setImage(#imageLiteral(resourceName: "dltBtnRed"), for: .normal)
        case 2:
            cell.imgVwTop.isHidden = true
            cell.imgVwDevice.image = #imageLiteral(resourceName: "imgxBox")
            cell.lblTitle.text = "Xbox One"
            cell.btnDelete.setImage(#imageLiteral(resourceName: "dltBtnRed"), for: .normal)

        
        default:
            break
        }
        
        return cell
    }
}

