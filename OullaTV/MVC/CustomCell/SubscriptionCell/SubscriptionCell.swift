//
//  SubscriptionCell.swift
//  OullaTV
//
//  Created by Sumit Sharma on 21/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class SubscriptionCell: UITableViewCell {

    @IBOutlet weak var labelHeading: UILabel!
    @IBOutlet weak var labelDetail1: UILabel!
    @IBOutlet weak var labelDetail2: UILabel!
    @IBOutlet weak var imgVwTop: UIImageView!
    @IBOutlet weak var labelDetail3: UILabel!
    @IBOutlet weak var imgVwBottom: UIImageView!
    @IBOutlet weak var labelPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
