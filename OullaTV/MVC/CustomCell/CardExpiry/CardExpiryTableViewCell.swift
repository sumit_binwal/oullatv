//
//  CardExpiryTableViewCell.swift
//  OullaTV
//
//  Created by MAC on 20/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class CardExpiryTableViewCell: UITableViewCell {

    @IBOutlet weak var expiryMonthTextField: UITextField!
    @IBOutlet weak var expiryYearTextField: UITextField!
    
    @IBOutlet weak var expiryMonthBgView: UIView!
    @IBOutlet weak var expiryMonthNameLabel: UILabel!
    
    @IBOutlet weak var expiryYearBgView: UIView!
    @IBOutlet weak var expiryYearNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
        
        expiryMonthNameLabel.text = "Expiry Month"
        expiryYearNameLabel.text = "Expiry Year"
        
        expiryYearTextField.placeholder = ""
        expiryMonthTextField.placeholder = ""
        
        expiryMonthBgView.backgroundColor = UIColor.init(red: 22.0/255.0, green: 52.0/255.0, blue: 62.0/255.0, alpha: 1.0)
        expiryYearBgView.backgroundColor = UIColor.init(red: 22.0/255.0, green: 52.0/255.0, blue: 62.0/255.0, alpha: 1.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
