//
//  LoginViewController.swift
//  OullaTV
//
//  Created by Sumit Sharma on 18/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var loginTableView: UITableView!
    @IBOutlet weak var signInButton: UIButton!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setUpView() {
        headerView.frame = CGRect.init(x: headerView.frame.origin.x, y: headerView.frame.origin.y, width: UIScreen.main.bounds.size.width, height: headerView.frame.size.height * scaleFactorX)
        
        footerView.frame = CGRect.init(x: footerView.frame.origin.x, y: footerView.frame.origin.y, width: UIScreen.main.bounds.size.width, height: footerView.frame.size.height * scaleFactorX)
        
        signInButton.layer.borderColor = UIColor.init(red: 142.0/255.0, green: 89.0/255.0, blue: 177.0/255.0, alpha: 1.0).cgColor
        signInButton.layer.borderWidth = 1.0
        signInButton.layer.masksToBounds = true
        signInButton.layer.cornerRadius = (signInButton.frame.size.height / 2) * scaleFactorX
        
        loginTableView.delegate = self
        loginTableView.dataSource = self
        
        loginTableView.register(UINib.init(nibName: "LoginTableViewCell", bundle: nil), forCellReuseIdentifier: "LoginTableViewCell")
    }

    @IBAction func forgotPasswordClick(_ sender: Any) {
    }
    
    @IBAction func signInButtonClick(_ sender: Any) {
        
        
        let dashboard = UIStoryboard.getMainStoryBoard().instantiateInitialViewController()
        APPDELEGATE.window?.rootViewController = dashboard
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension LoginViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoginTableViewCell") as! LoginTableViewCell
        
        var cellType = cell.cellType
        
        switch indexPath.row {
        case 0:
            cellType = .email
        
        case 1:
            cellType = .password
            
        default:
            cellType = .none
        }
        
        cell.cellType = cellType
        
        return cell
    }
}
