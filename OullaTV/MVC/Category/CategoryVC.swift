//
//  CategoryVC.swift
//  OuulaTv
//
//  Created by MAC on 21/07/19.
//  Copyright © 2019 Sevta. All rights reserved.
//

import UIKit

class CategoryVC: UIViewController {

    @IBOutlet var collectionViewHeader: UICollectionView!
    
    @IBOutlet var tableViewCategory: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        tableViewCategory.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
        tableViewCategory.register(UINib(nibName: "CategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "CategoryTableViewCell")
    }
}

extension CategoryVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewHeader {
            return CGSize(width: UIScreen.main.bounds.size.width, height: 50 * scaleFactorX)
        }
        else {
            return CGSize(width: 149 * scaleFactorX, height: 164 * scaleFactorX)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewHeader {
            return 1
        }
        else {
            return 5
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewHeader {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryHeaderCollectionViewCell", for: indexPath) as! CategoryHeaderCollectionViewCell
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
            
            cell.layer.cornerRadius = 3.0
            cell.clipsToBounds = true
            
            cell.labelLive.layer.cornerRadius = 3.0
            cell.labelLive.clipsToBounds = true
            
            if collectionView.tag == 100 {
                cell.labelLive.isHidden = false
            }
            else {
                cell.labelLive.isHidden = true
            }
            
            return cell
        }
    }
}


extension CategoryVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 237 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        
        cell.labelTitle.textColor = .white
        
        cell.collectionViewCell.delegate = self
        cell.collectionViewCell.dataSource = self
        
        cell.collectionViewCell.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        cell.collectionViewCell.tag = indexPath.row + 100
        
        switch indexPath.row {
            
        case 0:
            cell.labelTitle.text = "Live Now"
        
        case 1:
            cell.labelTitle.text = "Sub Category"
        
        case 2:
            cell.labelTitle.text = "Sub Category"
        
        default:
            break
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
}
