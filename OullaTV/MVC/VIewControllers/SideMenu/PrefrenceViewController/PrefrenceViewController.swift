//
//  PrefrenceViewController.swift
//  OullaTV
//
//  Created by Sumit Sharma on 20/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class PrefrenceViewController: UIViewController {

    @IBOutlet weak var tableVwPrfrence: UITableView!
    @IBOutlet weak var headerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
//        headerView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 140 * scaleFactorX)
//        tableVwPrfrence.tableHeaderView = headerView
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
extension PrefrenceViewController :  UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrefrenceCell") as! PrefrenceCell
        
        

        
        switch indexPath.row {
        case 0:
            cell.labelText.text = "Caribbean"
            break
        case 1:
            cell.labelText.text = "African"
            break
        case 2:
            cell.labelText.text = "Indian"
            break
        case 3:
            cell.labelText.text = "Chinese"
            break
        case 4:
            cell.labelText.text = "Eastern Europe"
            break
        case 5:
            cell.labelText.text = "Latin America"
            break
            
        default:
            break
        }
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            let prefrenceVC = UIStoryboard.getSideMenuStoryBoard().instantiateViewController(withIdentifier: "PrefrenceViewController") as! PrefrenceViewController
            self.navigationController?.pushViewController(prefrenceVC, animated: true)
        default:
            break
        }
        
    }
}
