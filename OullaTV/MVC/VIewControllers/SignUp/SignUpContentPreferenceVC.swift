//
//  SignUpContentPreferenceVC.swift
//  OullaTV
//
//  Created by MAC on 20/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class SignUpContentPreferenceVC: UIViewController {

    @IBOutlet weak var signUpTableView: UITableView!
    @IBOutlet weak var proceedButton: UIButton!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    
    let section1PreferensesArr: [String] = ["Video Podcasts", "Comedy", "Streaming Concerts \"Oncerts\"", "Health & Wellness", "Original Series", "Movies", "Documentaries", "Internal Events/ Occurrences", "FIlm Festivals", "Awards", "Sports", "Entertainment", "Documentaries", "Indie films"]
    
    let section2PreferensesArr: [String] = ["Caribbean", "African", "Indian", "Chinese", "Eastern Europe", "Latin America"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setUpView() {
        
        headerView.frame = CGRect.init(x: headerView.frame.origin.x, y: headerView.frame.origin.y, width: UIScreen.main.bounds.size.width, height: headerView.frame.size.height * scaleFactorX)
        
        footerView.frame = CGRect.init(x: footerView.frame.origin.x, y: footerView.frame.origin.y, width: UIScreen.main.bounds.size.width, height: footerView.frame.size.height * scaleFactorX)
        
        proceedButton.layer.borderColor = UIColor.init(red: 142.0/255.0, green: 89.0/255.0, blue: 177.0/255.0, alpha: 1.0).cgColor
        proceedButton.layer.borderWidth = 1.0
        proceedButton.layer.masksToBounds = true
        proceedButton.layer.cornerRadius = (proceedButton.frame.size.height / 2) * scaleFactorX
        
        signUpTableView.delegate = self
        signUpTableView.dataSource = self
    }
    
    @IBAction func proceedButtonClick(_ sender: Any) {
        let subcriptionTypeVC = UIStoryboard.getSignUpStoryBoard().instantiateViewController(withIdentifier: "SubcriptionTypeVC") as! SubcriptionTypeVC
        self.navigationController?.pushViewController(subcriptionTypeVC, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension SignUpContentPreferenceVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "ContentPreferenceHeaderTableViewCell") as! ContentPreferenceHeaderTableViewCell
        
        headerCell.contentView.backgroundColor = UIColor.init(red: 44.0/255.0, green: 88.0/255.0, blue: 103.0/255.0, alpha: 1.0)
        
        if section == 0 {
            headerCell.headerLabel.text = "CATEGORIES"
            headerCell.seperatorLabel.isHidden = true
            headerCell.seperatorLabelTopConstrainst.constant = 0
            headerCell.headerLabelTopConstrainst.constant = 20
        }
        else {
            headerCell.headerLabel.text = "Enthnic REGION".uppercased()
            headerCell.seperatorLabel.isHidden = false
            headerCell.seperatorLabelTopConstrainst.constant = 50
            headerCell.headerLabelTopConstrainst.constant = 32
        }
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 50 * scaleFactorX
        }
        else {
            return 120 * scaleFactorX
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return section1PreferensesArr.count
        }
        else {
            return section2PreferensesArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContentPreferenceTableViewCell") as! ContentPreferenceTableViewCell
        
        if indexPath.section == 0 {
            cell.nameLabel.text = section1PreferensesArr[indexPath.row]
        }
        else {
            cell.nameLabel.text = section2PreferensesArr[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44 * scaleFactorX
    }
}
