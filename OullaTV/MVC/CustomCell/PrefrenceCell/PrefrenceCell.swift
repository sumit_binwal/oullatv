//
//  PrefrenceCell.swift
//  OullaTV
//
//  Created by Sumit Sharma on 20/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class PrefrenceCell: UITableViewCell {

    @IBOutlet weak var labelText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelText.font = UIFont.init(name: labelText.font.fontName, size: labelText.font.pointSize * scaleFactorX)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
