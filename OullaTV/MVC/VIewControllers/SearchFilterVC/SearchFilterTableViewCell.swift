
//
//  SearchFilterTableViewCell.swift
//  OuulaTv
//
//  Created by Sevta on 21/07/19.
//  Copyright © 2019 Sevta. All rights reserved.
//

import UIKit

class SearchFilterTableViewCell: UITableViewCell {
    @IBOutlet var viewBg: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewBg.layer.cornerRadius = 3.0
        viewBg.clipsToBounds      = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
