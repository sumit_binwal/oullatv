//
//  HeaderFMTableViewCell.swift
//  OuulaTv
//
//  Created by Sevta on 21/07/19.
//  Copyright © 2019 Sevta. All rights reserved.
//

import UIKit

class HeaderFMTableViewCell: UITableViewCell {

    @IBOutlet var imageViewExpand: UIImageView!
    @IBOutlet var labelTitleSection: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setExpanded() {
        imageViewExpand.image = #imageLiteral(resourceName: "expand_less")
    }
    
    func setCollapsed() {
        imageViewExpand.image = #imageLiteral(resourceName: "expand_more")
    }
    
}
